DROP DATABASE IF EXISTS airline_db;
CREATE DATABASE IF NOT EXISTS airline_db;
USE airline_db;

CREATE TABLE IF NOT EXISTS Airline(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS Flight(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idAirline INT UNSIGNED,
    flightNumber INT UNIQUE,
    origin VARCHAR(255),
    destiny VARCHAR(255),
    FOREIGN KEY(idAirline) REFERENCES Airline(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS Passenger(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idFlight INT,
    name VARCHAR(255) UNIQUE,
    phone VARCHAR(255),
    email VARCHAR(255)
);


INSERT INTO Airline(id,name) VALUES (1,'VUELO1'),(2,'VUELO2');
INSERT INTO Flight(id,idAirline,flightNumber,origin,destiny)
VALUES (1,1,101,'A','B'),(2,2,100,'C','D');

INSERT INTO Passenger(id,idFlight,name,phone,email)
VALUES (1,1,'Thom','123456789','thomtwd@gmail.com'),
(2,1,'Pepe','987654321','pep@gmail.com'),
(3,2,'Ricardo','999999999','ricardo@gmail.com'),
(4,2,'Rafael','987456325','rafa@gmail.com');

DELIMITER $$
CREATE PROCEDURE SP_CREATE_AIRLINE(
    IN aname VARCHAR(255)
)
BEGIN
    DECLARE id_generated INT;
    INSERT INTO Airline (name) VALUES (aname);
    SET id_generated = LAST_INSERT_ID();
    SELECT * FROM Airline WHERE id = id_generated;
END$$

CREATE PROCEDURE SP_CREATE_FLIGHT(
    IN flightNumber INT,
    IN origin VARCHAR(255),
    IN destiny VARCHAR(255),
    IN idAirline INT
)
BEGIN
    DECLARE id_generated INT;
    INSERT INTO Flight(idAirline,flightNumber,origin,destiny)
    VALUES (idAirline,flightNumber,origin,destiny);
    SET id_generated = LAST_INSERT_ID();
    SELECT * FROM Flight WHERE id = id_generated;
END$$

CREATE PROCEDURE SP_CREATE_PASSENGER(
    IN name VARCHAR(255),
    IN phone VARCHAR(255),
    IN email VARCHAR(255)
)
BEGIN
    DECLARE id_generated INT;
    INSERT INTO Passenger(name,phone,email)
    VALUES (name,phone,email);
    SET id_generated = LAST_INSERT_ID();
    SELECT * FROM Passenger WHERE id = id_generated;
END$$

CREATE PROCEDURE SP_ASSIGN_PASSENGER(
    IN p_name VARCHAR(255),
    IN p_flightNumber INT
)
BEGIN

    DECLARE v_idFlight INT;

    SELECT id INTO v_idFlight
    FROM Flight
    WHERE flightNumber = p_flightNumber;

    UPDATE Passenger
    SET idFlight = v_idFlight
    WHERE name = p_name;

    SELECT * FROM Passenger WHERE name = p_name;
END$$
DELIMITER ;