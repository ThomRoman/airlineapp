package com.airlineapp.airlineapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AssignPassengerDTO {
	private String passengerName;
	private int flightNumber;
}
