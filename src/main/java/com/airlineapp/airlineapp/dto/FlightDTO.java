package com.airlineapp.airlineapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FlightDTO {
	private int flightNumber;
	private String origin;
	private String destiny;
	private int idAirline;
}
