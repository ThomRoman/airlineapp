package com.airlineapp.airlineapp.repository;

import com.airlineapp.airlineapp.dto.FlightDTO;
import com.airlineapp.airlineapp.model.Flight;

import java.util.List;

public interface FlightRepository {
	List<Flight> getFlights();
	Flight createFlight(FlightDTO flightDTO);
	Flight getFlightByFlightNumber(int flightNumber);

	Flight getFlightById(int id);
}
