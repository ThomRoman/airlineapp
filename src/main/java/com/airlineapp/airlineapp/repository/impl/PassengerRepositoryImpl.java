package com.airlineapp.airlineapp.repository.impl;

import com.airlineapp.airlineapp.dto.AssignPassengerDTO;
import com.airlineapp.airlineapp.dto.PassengerDTO;
import com.airlineapp.airlineapp.model.Flight;
import com.airlineapp.airlineapp.model.Passenger;
import com.airlineapp.airlineapp.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PassengerRepositoryImpl implements PassengerRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public List<Passenger> getPassengers() {
		String sql = "SELECT * FROM Passenger";
		List<Passenger> passengers = jdbcTemplate.query(sql,(rs, rowNum) -> mapToPassenger(rs));
		return passengers;
	}

	public Passenger mapToPassenger(ResultSet rs) throws SQLException {
		Passenger passenger = new Passenger();
		passenger.setId(rs.getInt("id"));
		passenger.setEmail(rs.getString("email"));
		passenger.setName(rs.getString("name"));
		passenger.setPhone(rs.getString("phone"));
		Flight flight = new Flight();
		flight.setId(rs.getInt("idFlight"));
		passenger.setFlight(flight);
		return passenger;
	}

	@Override
	public Passenger getPassengerByName(String name) {
		String sql = "SELECT * FROM Passenger WHERE name = ?";
		Passenger passenger = jdbcTemplate.queryForObject(sql,(rs, rowNum) -> mapToPassenger(rs),name);
		return passenger;
	}

	@Override
	public Passenger createPassenger(PassengerDTO passengerDTO) {
		String sql = "CALL SP_CREATE_PASSENGER(?,?,?)";
		Object[] params = new Object[] {
				passengerDTO.getName(),
				passengerDTO.getPhone(),
				passengerDTO.getEmail()
		};
		Passenger passenger = jdbcTemplate.queryForObject(sql,params,(rs, rowNum) -> mapToPassenger(rs));
		return passenger;
	}

	@Override
	public Passenger assignPassenger(AssignPassengerDTO assignPassengerDTO) {
		String sql = "CALL SP_ASSIGN_PASSENGER(?,?)";
		Object[] params = new Object[] {
				assignPassengerDTO.getPassengerName(),
				assignPassengerDTO.getFlightNumber(),
		};
		Passenger passenger = jdbcTemplate.queryForObject(sql,params,(rs, rowNum) -> mapToPassenger(rs));
		return passenger;
	}
}
