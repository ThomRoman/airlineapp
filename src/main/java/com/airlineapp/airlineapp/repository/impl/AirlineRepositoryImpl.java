package com.airlineapp.airlineapp.repository.impl;

import com.airlineapp.airlineapp.dto.AirlineDTO;
import com.airlineapp.airlineapp.model.Airline;
import com.airlineapp.airlineapp.repository.AirlineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AirlineRepositoryImpl implements AirlineRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Airline> getAirlines() {
		String sql = "SELECT * FROM Airline";
		List<Airline> airlines = jdbcTemplate.query(sql,(rs, rowNum) -> mapToAirline(rs));
		return airlines;
	}

	public Airline mapToAirline(ResultSet rs) throws SQLException {
		Airline airline = new Airline(
				rs.getInt("id"),
				rs.getString("name")
		);
		return airline;
	}

	@Override
	public Airline createAirline(AirlineDTO airlineDTO) {
		String sql = "CALL SP_CREATE_AIRLINE(?)";
		Airline airline = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToAirline(rs), airlineDTO.getName());
		return airline;
	}

	@Override
	public Airline getAirlineById(Integer id) {
		String sql = "SELECT * FROM Airline WHERE id = ?";
		Airline airline = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToAirline(rs),id);
		return airline;
	}
}
