package com.airlineapp.airlineapp.repository.impl;

import com.airlineapp.airlineapp.dto.FlightDTO;
import com.airlineapp.airlineapp.model.Airline;
import com.airlineapp.airlineapp.model.Flight;
import com.airlineapp.airlineapp.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class FlightRepositoryImpl implements FlightRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Flight> getFlights() {
		String sql = "SELECT * FROM Flight";
		List<Flight> flights = jdbcTemplate.query(sql,(rs, rowNum) -> mapToFlight(rs));
		return flights;
	}

	public Flight mapToFlight(ResultSet rs) throws SQLException {
		Flight flight = new Flight();
		flight.setId(rs.getInt("id"));
		flight.setFlightNumber(rs.getInt("flightNumber"));
		flight.setOrigin(rs.getString("origin"));
		flight.setDestiny(rs.getString("destiny"));
		Airline airline = new Airline();
		airline.setId(rs.getInt("idAirline"));
		flight.setAirline(airline);
		return flight;
	}


	@Override
	public Flight createFlight(FlightDTO flightDTO) {
		String sql = "CALL SP_CREATE_FLIGHT(?,?,?,?)";
		Object[] params = new Object[] {
				flightDTO.getFlightNumber(),
				flightDTO.getOrigin(),
				flightDTO.getDestiny(),
				flightDTO.getIdAirline()
		};
		Flight flight = jdbcTemplate.queryForObject(sql,params,(rs, rowNum) -> mapToFlight(rs));
		return flight;
	}

	@Override
	public Flight getFlightByFlightNumber(int flightNumber) {
		String sql = "SELECT * FROM Flight WHERE flightNumber = ? LIMIT 1";
		Flight flight = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToFlight(rs), flightNumber);
		return flight;
	}

	@Override
	public Flight getFlightById(int id) {
		String sql = "SELECT * FROM Flight WHERE id = ? ";
		Flight flight = jdbcTemplate.queryForObject(sql,
				(rs, rowNum) -> mapToFlight(rs), id);
		return flight;
	}
}
