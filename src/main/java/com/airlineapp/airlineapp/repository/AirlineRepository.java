package com.airlineapp.airlineapp.repository;

import com.airlineapp.airlineapp.dto.AirlineDTO;
import com.airlineapp.airlineapp.model.Airline;

import java.util.List;

public interface AirlineRepository {
	List<Airline>  getAirlines();
	Airline createAirline(AirlineDTO airlineDTO);
	Airline getAirlineById(Integer id);
}
