package com.airlineapp.airlineapp.controller;

import com.airlineapp.airlineapp.dto.AssignPassengerDTO;
import com.airlineapp.airlineapp.dto.PassengerDTO;
import com.airlineapp.airlineapp.model.Flight;
import com.airlineapp.airlineapp.model.Passenger;
import com.airlineapp.airlineapp.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/passengers")
public class PassengerController {

	@Autowired
	private PassengerService passengerService;

	@GetMapping
	public ResponseEntity<List<Passenger>> getPassengers(){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(passengerService.getPassengers());
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{name}")
	public ResponseEntity<Passenger> getPassengerByName(@PathVariable String name){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(passengerService.getPassengerByName(name));
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping
	public ResponseEntity<Passenger> createPassenger(@RequestBody PassengerDTO passengerDTO){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(passengerService.createPassenger(passengerDTO));
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("/assign")
	public ResponseEntity<Passenger> assignPassenger(@RequestBody AssignPassengerDTO assignPassengerDTO){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(passengerService.assignPassenger(assignPassengerDTO));
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			return ResponseEntity.internalServerError().build();
		}
	}
}
