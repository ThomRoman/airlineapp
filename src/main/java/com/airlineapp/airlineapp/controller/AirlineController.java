package com.airlineapp.airlineapp.controller;


import com.airlineapp.airlineapp.dto.AirlineDTO;
import com.airlineapp.airlineapp.model.Airline;
import com.airlineapp.airlineapp.service.AirlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/airlines")
public class AirlineController {

	@Autowired
	private AirlineService airlineService;

	@GetMapping
	public ResponseEntity<List<Airline>> getAirlines(){
		try {
			List<Airline> airlinesResponse = airlineService.getAirlines();
			return ResponseEntity.status(HttpStatus.OK).body(airlinesResponse);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping
	public ResponseEntity<Airline> createAirline(@RequestBody AirlineDTO airlineDTO){
		try {
			Airline airlineCreatedResponse = airlineService.createAirline(airlineDTO);
			return ResponseEntity.status(HttpStatus.OK).body(airlineCreatedResponse);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<Airline> getAirlineById(@PathVariable Integer id){
		try {
			Airline airlineFoundResponse = airlineService.getAirlineById(id);
			return ResponseEntity.status(HttpStatus.OK).body(airlineFoundResponse);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}
}
