package com.airlineapp.airlineapp.controller;

import com.airlineapp.airlineapp.dto.FlightDTO;
import com.airlineapp.airlineapp.model.Flight;
import com.airlineapp.airlineapp.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/flights")
public class FlightController {

	@Autowired
	private FlightService flightService;

	@GetMapping
	public ResponseEntity<List<Flight>> getFlights(){
		try {

			var response = flightService.getFlights();

			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping
	public ResponseEntity<Flight> createFlight(@RequestBody FlightDTO flightDTO){
		try {

			var response = flightService.createFlight(flightDTO);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/{number}")
	public ResponseEntity<Flight> getFlightByFlightNumber(@PathVariable Integer number){
		try {
			var response = flightService.getFlightByFlightNumber(number);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		catch(Exception ex) {
			return ResponseEntity.internalServerError().build();
		}
	}
}
