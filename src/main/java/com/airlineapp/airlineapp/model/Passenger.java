package com.airlineapp.airlineapp.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Passenger {
	private int id;
	private Flight flight;
	private String name;
	private String phone;
	private String email;
}
