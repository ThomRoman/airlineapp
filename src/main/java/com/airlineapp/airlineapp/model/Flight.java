package com.airlineapp.airlineapp.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Flight {
	private int id;
	private Airline airline;
	private int flightNumber;
	private String origin;
	private String destiny;
}
