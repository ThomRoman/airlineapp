package com.airlineapp.airlineapp.service.impl;

import com.airlineapp.airlineapp.dto.AirlineDTO;
import com.airlineapp.airlineapp.model.Airline;
import com.airlineapp.airlineapp.repository.AirlineRepository;
import com.airlineapp.airlineapp.service.AirlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirlineServiceImpl implements AirlineService {

	@Autowired
	private AirlineRepository airlineRepository;

	@Override
	public List<Airline> getAirlines() {
		return airlineRepository.getAirlines();
	}

	@Override
	public Airline createAirline(AirlineDTO airlineDTO) {
		return airlineRepository.createAirline(airlineDTO);
	}

	@Override
	public Airline getAirlineById(Integer id) {
		return airlineRepository.getAirlineById(id);
	}
}
