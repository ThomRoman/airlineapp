package com.airlineapp.airlineapp.service.impl;

import com.airlineapp.airlineapp.dto.AssignPassengerDTO;
import com.airlineapp.airlineapp.dto.PassengerDTO;
import com.airlineapp.airlineapp.model.Passenger;
import com.airlineapp.airlineapp.repository.AirlineRepository;
import com.airlineapp.airlineapp.repository.FlightRepository;
import com.airlineapp.airlineapp.repository.PassengerRepository;
import com.airlineapp.airlineapp.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService {


	@Autowired
	private PassengerRepository passengerRepository;

	@Autowired
	private FlightRepository flightRepository;

	@Autowired
	private AirlineRepository airlineRepository;


	@Override
	public List<Passenger> getPassengers() {
		var pas =  passengerRepository.getPassengers();
		for(var pa  : pas){
			if(pa.getFlight().getId() == 0){
				pa.setFlight(null);
				continue;
			}
			var fl = flightRepository.getFlightById(pa.getFlight().getId());
			var ar = airlineRepository.getAirlineById(fl.getAirline().getId());

			fl.setAirline(ar);
			pa.setFlight(fl);
		}
		return pas;
	}

	@Override
	public Passenger getPassengerByName(String name) {
		var pa = passengerRepository.getPassengerByName(name);
		if(pa.getFlight().getId() != 0) {
			var fl = flightRepository.getFlightById(pa.getFlight().getId());
			var ar = airlineRepository.getAirlineById(fl.getAirline().getId());

			fl.setAirline(ar);
			pa.setFlight(fl);
		}else{
			pa.setFlight(null);

		}

		return pa;
	}

	@Override
	public Passenger createPassenger(PassengerDTO passengerDTO) {
		var pa =  passengerRepository.createPassenger(passengerDTO);
		pa.setFlight(null);
		return pa;
	}

	@Override
	public Passenger assignPassenger(AssignPassengerDTO assignPassengerDTO) {
		var pa =  passengerRepository.assignPassenger(assignPassengerDTO);
		var fl = flightRepository.getFlightById(pa.getFlight().getId());
		var ar = airlineRepository.getAirlineById(fl.getAirline().getId());

		fl.setAirline(ar);
		pa.setFlight(fl);
		return pa;
	}
}
