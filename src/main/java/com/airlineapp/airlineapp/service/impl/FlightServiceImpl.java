package com.airlineapp.airlineapp.service.impl;

import com.airlineapp.airlineapp.dto.FlightDTO;
import com.airlineapp.airlineapp.model.Airline;
import com.airlineapp.airlineapp.model.Flight;
import com.airlineapp.airlineapp.repository.AirlineRepository;
import com.airlineapp.airlineapp.repository.FlightRepository;
import com.airlineapp.airlineapp.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

	@Autowired
	private AirlineRepository airlineRepository;

	@Autowired
	private FlightRepository flightRepository;

	@Override
	public List<Flight> getFlights() {

		List<Flight> flights = flightRepository.getFlights();

		for (Flight flight: flights){
			Airline airline = airlineRepository.getAirlineById(flight.getAirline().getId());
			flight.setAirline(airline);
		}

		return flights;
	}

	@Override
	public Flight createFlight(FlightDTO flightDTO) {
		Flight flight = flightRepository.createFlight(flightDTO);
		Airline airline = airlineRepository.getAirlineById(flight.getAirline().getId());
		flight.setAirline(airline);
		return  flight;
	}

	@Override
	public Flight getFlightByFlightNumber(int flightNumber) {
		Flight flight = flightRepository.getFlightByFlightNumber(flightNumber);
		Airline airline = airlineRepository.getAirlineById(flight.getAirline().getId());
		flight.setAirline(airline);
		return  flight;
	}
}
