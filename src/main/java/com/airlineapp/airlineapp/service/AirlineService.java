package com.airlineapp.airlineapp.service;

import com.airlineapp.airlineapp.dto.AirlineDTO;
import com.airlineapp.airlineapp.model.Airline;

import java.util.List;

public interface AirlineService {
	List<Airline> getAirlines();
	Airline createAirline(AirlineDTO airlineDTO);
	Airline getAirlineById(Integer id);
}
