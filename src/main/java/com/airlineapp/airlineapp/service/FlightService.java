package com.airlineapp.airlineapp.service;

import com.airlineapp.airlineapp.dto.FlightDTO;
import com.airlineapp.airlineapp.model.Flight;

import java.util.List;

public interface FlightService {
	List<Flight> getFlights();
	Flight createFlight(FlightDTO flightDTO);
	Flight getFlightByFlightNumber(int flightNumber);
}
