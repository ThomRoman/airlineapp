package com.airlineapp.airlineapp.service;

import com.airlineapp.airlineapp.dto.AssignPassengerDTO;
import com.airlineapp.airlineapp.dto.PassengerDTO;
import com.airlineapp.airlineapp.model.Passenger;

import java.util.List;

public interface PassengerService {
	List<Passenger> getPassengers();
	Passenger getPassengerByName(String name);
	Passenger createPassenger(PassengerDTO passengerDTO);

	Passenger assignPassenger(AssignPassengerDTO assignPassengerDTO);
}
