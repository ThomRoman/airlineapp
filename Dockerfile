FROM openjdk:11

WORKDIR /app

COPY ./target/airlineapp-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "airlineapp-0.0.1-SNAPSHOT.jar"]

# mvn package o mvn clean package
# docker build .